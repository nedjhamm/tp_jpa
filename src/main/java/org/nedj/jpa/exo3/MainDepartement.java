package org.nedj.jpa.exo3;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class MainDepartement {

	public static void main(String[] args)  throws IOException {
		
		
		// with create in persistence 
		
		
		String file="src/main/resources/files/departements.csv";
		//Path path= Paths.get(file);
	
		String PersistenceUnitName="tp-jpa-hibernate-create";
		//point d entree de jpa
		EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory(PersistenceUnitName);
		System.out.println(entityManagerFactory);
		EntityManager entityManager= entityManagerFactory.createEntityManager();
		
		
		DepartementDBService cs=new DepartementDBImpl(entityManager);
		
		//La base est grande elle prend beaucoup de temps pour se charger
		//je me limite � un petit nombre 
		//importer
		entityManager.getTransaction().begin();
		
		DepartementImporter ci= new DepartementCSVImporter(cs);
		ci.importDepartements(file);

		entityManager.getTransaction().commit();
		//select avec un id
		System.out.println("Departement avec id=93: "+cs.getDepartementById(93L));
		
		//select avec un name
		System.out.println("Departement avec nom=Seine-Saint-Denis: "+cs.getDepartementByName("Seine-Saint-Denis"));
		
	}
	

}
