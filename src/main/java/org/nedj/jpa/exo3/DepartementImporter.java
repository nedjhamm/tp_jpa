package org.nedj.jpa.exo3;


import java.util.List;

public interface DepartementImporter {
	
	void importDepartements(String path);
}
