package org.nedj.jpa.exo3;


import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DepartementCSVImporter implements DepartementImporter {

	private DepartementDBService depService;
	public DepartementCSVImporter(DepartementDBService depService)
	{
		this.depService = depService;
	}
	
	
	@Override
	public void importDepartements(String path) {
		Path opPath= Paths.get(path);
		int i;
		// MalformedInputException:: did not accept UTF-8 encoding 
		try (Stream<String>lines=Files.lines(opPath, StandardCharsets.ISO_8859_1))
		{
			List<Departement> mesDepartements=lines.map(convertStoC)
			.collect(Collectors.toList());
			
			
			for (Departement d : mesDepartements)
			{
				depService.writeDepartement(d);
				System.out.println("...\n");
			}
			//too slow
			System.out.println("done");
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
	
		
		
		}
	
	Function<String, Departement> convertStoC= s->{
		Departement c=new Departement();
		c.setCodePostal(s.split(";")[0]);
		c.setNomDepar(s.split(";")[1]);
		
		return c;
};

	}


