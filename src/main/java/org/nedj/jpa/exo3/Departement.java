package org.nedj.jpa.exo3;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity(name="Departement")

@NamedQueries(
		{
		@NamedQuery(name="Departement.byName",query="select d from Departement d where d.nomDepar=:name")
		})


@Table(name="Departement")

public class Departement implements Serializable {


	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name="code_postal" ,length=40)
	private String codePostal;
	
	@Column(name="nom_departement" , length= 40)
	private String nomDepar;

	
	public Departement(String codePostal, String nomDepar) {
		this.codePostal = codePostal;
		this.nomDepar = nomDepar;
	}
	
	

	public Departement() {
	}



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getNomDepar() {
		return nomDepar;
	}

	public void setNomDepar(String nomDepar) {
		this.nomDepar = nomDepar;
	}

	@Override
	public String toString() {
		return "Departement [id=" + id + ", codePostal=" + codePostal + ", nomDepar=" + nomDepar + "]";
	}
	
	
}
