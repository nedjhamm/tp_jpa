package org.nedj.jpa.exo3;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class CreateTableDepartement {

	public static void main(String[] args) {
		
		
		String PersistenceUnitName="tp-jpa-hibernate-create";
		EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory(PersistenceUnitName);
		System.out.println(entityManagerFactory);
	}

}
