package org.nedj.jpa.exo3;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;



public class DepartementDBImpl implements DepartementDBService {
	
	private EntityManager entityManager;

	public DepartementDBImpl(EntityManager entityManager) {
		this.entityManager=entityManager;
	}

	@Override
	public boolean writeDepartement(Departement departement) {
		
		
			entityManager.persist(departement);
		
		
		return true;
	}

	@Override
	public Departement getDepartementById(long id) {
		
		Departement departement=entityManager.find(Departement.class, id);
		
		return departement;
	}

	@Override
	public Departement getDepartementByName(String name) {
		
		Query query= entityManager.createNamedQuery("Departement.byName");
		query.setParameter("name", name);
		return (Departement) query.getSingleResult();
		
	}



}
