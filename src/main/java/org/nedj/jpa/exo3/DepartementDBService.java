package org.nedj.jpa.exo3;

import java.util.List;

public interface DepartementDBService {

	boolean writeDepartement(Departement departement);
	Departement getDepartementById(long id);
	Departement getDepartementByName(String name);
	
}
