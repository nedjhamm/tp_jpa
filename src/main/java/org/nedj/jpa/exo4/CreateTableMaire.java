package org.nedj.jpa.exo4;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class CreateTableMaire {

	public static void main(String[] args) {
		
		
		String PersistenceUnitName="tp-jpa-hibernate-create-maire";
		EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory(PersistenceUnitName);
		System.out.println(entityManagerFactory);
	}

}
