package org.nedj.jpa.exo4;


import java.util.List;

public interface MaireImporter {
	
	void importMaires(String path);
}
