package org.nedj.jpa.exo4;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;


// il s avere qu il y a des communes d�finies en double, triple.. sur la base de donn�es
// les retours de singleResult affiche des erreurs de not unique result


public class MaireDBImpl implements MaireDBService {
	
	private EntityManager entityManager;

	public MaireDBImpl(EntityManager entityManager) {
		this.entityManager=entityManager;
	}

	@Override
	public boolean writeMaire(Maire maire) {
		

			entityManager.persist(maire);

		
		return true;
	}

	@Override
	public Maire getMaireById(long id) {
		
		Maire maire=entityManager.find(Maire.class, id);
		System.out.println("dbImpl "+ maire.toString().isEmpty());
		return maire;
	}

	@Override
	public Maire getMaireByName(String name, String fullname) {
		
		Query query= entityManager.createNamedQuery("Maire.byName");
		query.setParameter("name", name);
		query.setParameter("fullname", fullname);
		return (Maire) query.getSingleResult();
		
	}

	@Override
	public int countMaireByCP(String libdpt) {
		
	
		Query query= entityManager.createNamedQuery("Maire.byDep");
		query.setParameter("dep",libdpt);
		List<Maire> maires= (List<Maire>) query.getResultList();
		if(maires.size()!=0)
		return maires.size();
		else
		{
			System.out.println("Le d�partement n'existe pas");
			return 0;
		}
	}
	
	@Override
	public Maire getMaireByCommune(String libsubcom)
	{
		Query query= entityManager.createNamedQuery("Maire.byCommune");
		query.setParameter("commune", libsubcom);
		List<Maire> list= (List<Maire>) query.getResultList();
		if(list.size()!=0)
			return list.get(0);
				// query.getSingleResult();
		else
			return new Maire();
		
	}
	
	@Override
	public List<Maire> getMaires()
	{
		Query query= entityManager.createNamedQuery("Maire.all");
		List<Maire> maires= (List<Maire>) query.getResultList();
		return maires;
		
	}
	
	
	

}
