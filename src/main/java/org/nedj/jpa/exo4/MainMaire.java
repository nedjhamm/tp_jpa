package org.nedj.jpa.exo4;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.FlushModeType;
import javax.persistence.Persistence;

import org.nedj.jpa.exo2.Commune;
import org.nedj.jpa.exo2.CommuneCSVImporter;
import org.nedj.jpa.exo2.CommuneDBImpl;
import org.nedj.jpa.exo2.CommuneDBService;
import org.nedj.jpa.exo2.CommuneImporter;

public class MainMaire {

	public static void main(String[] args)  throws IOException {
		
		
		// with create in persistence 
		
		String fileMaire="src/main/resources/files/maires.csv";
		String fileCommune="src/main/resources/files/laposte_hexasmal.csv";
		
		String PersistenceUnitName="tp-jpa-hibernate-create-maire";
		//point d entree de jpa
		EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory(PersistenceUnitName);
		System.out.println(entityManagerFactory);
		EntityManager entityManager= entityManagerFactory.createEntityManager();
		
		
		MaireDBService ms=new MaireDBImpl(entityManager);
		CommuneDBService cs=new CommuneDBImpl(entityManager);
		
		//La base est grande elle prend beaucoup de temps pour se charger
		//je me limite � un petit nombre 
		//importer
		entityManager.getTransaction().begin();
		
		MaireImporter mi= new MaireCSVImporter(ms);
		mi.importMaires(fileMaire);
		
		CommuneImporter ci= new CommuneCSVImporter(cs);
		ci.importCommunes(fileCommune);

		entityManager.getTransaction().commit();
		
		// cette base de donn�e les champs sont en double quotes( la plupart)
		//select avec un id
		System.out.println("Maire avec id=10: "+ms.getMaireById(10L));
		
		//select avec un name
		System.out.println("Maire Annie Bionda: "+ms.getMaireByName("\"Annie\"","\"BIONDA\""));
		
		//compter le nombre de communes
		//the doubles quotes in this DB ...
		System.out.println("Nombre de maires avec dans le d�partement AIN: "+ms.countMaireByCP("\"AIN\""));
	
		//entityManager.setFlushMode(FlushModeType.COMMIT);
		System.out.println("Le maire d'Argis: "+ms.getMaireByCommune("\"Argis\""));
	

		entityManager.getTransaction().begin();
		//recuperer les maires
		List<Maire> maires =ms.getMaires();
		maires.stream().limit(20).forEach(System.out::println);
		List<Commune>communes=cs.getCommunes();
		
		//set
	
		communes.forEach(
				c-> 
				{
				
					Maire m=ms.getMaireByCommune("\""+c.getNomCommune()+"\"");
					
					if(m.getId()!=0)
					{
						c.setMaire(m);
						m.setCommune(c);
						entityManager.persist(c);
						entityManager.persist(m);
					}
					
				}
				);
		
		entityManager.getTransaction().commit();
		System.out.println("finish");
		
	}
	

}
