package org.nedj.jpa.exo4;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.nedj.jpa.exo4.Civpsn;

public class MaireCSVImporter implements MaireImporter {

	private MaireDBService comService;
	public MaireCSVImporter(MaireDBService comService)
	{
		this.comService = comService;
	}
	
	
	@Override
	public void importMaires(String path) {
		Path opPath= Paths.get(path);
		int i;
		try (Stream<String>lines=Files.lines(opPath))
		{
			//if used, for quick results, i limited the treatement to a small portion  
			List<Maire> mesMaires=lines.skip(1).map(convertStoC)
					
			.collect(Collectors.toList());
			
			
			for (Maire c : mesMaires)
			{
				comService.writeMaire(c);
				System.out.println("...\n");
			}
			//too slow
			System.out.println("done");
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
	
		
		
		}
	
	Function<String, Maire> convertStoC= s->{
		Maire c=new Maire();
		
		c.setLibdpt(s.split(";")[1]);
		c.setLibsubcom(s.split(";")[2].toUpperCase().replace("-", " ").replace("L'", ""));
	
		c.setNompsn(s.split(";")[4]);
		c.setPrepsn(s.split(";")[5]);
		
		//sounds like split returns a ""M"" and because of that, I couldn t directly transmit it to the enum 
		String civ= (s.split(";")[6].equals("\"M\""))? "M": "F";    
		
		c.setCivpsn(Civpsn.valueOf(civ));

		System.out.println(c.getCivpsn());
		//c.setNaissance(Date.parse(s.split(";")[7]));
		c.setNaissance(new Date());
		System.out.println(c.getNaissance());
		c.setLibcsp(s.split(";")[9]);
		
		return c;
};

	}


