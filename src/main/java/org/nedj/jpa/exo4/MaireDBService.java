package org.nedj.jpa.exo4;

import java.util.List;

public interface MaireDBService {

	boolean writeMaire(Maire maire);
	Maire getMaireById(long id);
	Maire getMaireByName(String name, String fullname);
	public int countMaireByCP(String libdpt);
	public Maire getMaireByCommune(String libsubcom);
	public List<Maire> getMaires();
}
