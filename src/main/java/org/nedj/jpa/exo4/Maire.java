package org.nedj.jpa.exo4;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.nedj.jpa.exo2.Commune;
import org.nedj.jpa.exo4.Civpsn;


@Entity(name="Maire")

@NamedQueries(
		{
		@NamedQuery(name="Maire.byName",query="select c from Maire c where c.nompsn=:fullname and c.prepsn=:name"),
		@NamedQuery(name="Maire.byDep",query="select c from Maire c where c.libdpt=:dep"),
		@NamedQuery(name="Maire.byCommune", query="select c from Maire c where c.libsubcom=:commune"),
		@NamedQuery(name="Maire.all", query="select c from Maire c")
		})

@Table(name="Maire")
public class Maire implements Serializable{

	@OneToOne(mappedBy= "maire")
	private Commune commune;
	
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	
	
	@Column(name = "libdpt",length = 40)
	private String libdpt;
	
	@Column(name = "libsubcom",length = 100)
	private String libsubcom;
	

	
	@Column(name = "nompsn",length = 40)
	private String nompsn;
	
	@Column(name = "prepsn",length = 40)
	private String prepsn;
	
	@Enumerated(EnumType.STRING)
	@Column(length= 3)
	private Civpsn civpsn;
	
	@Temporal(TemporalType.DATE)
	private Date naissance;
	

	
	@Column(name = "libcsp",length = 100)
	private String libcsp;
	
	
	

	public Maire() {
	}

	public Maire( String libdpt, String libsubcom,  String nompsn, String prepsn,
			Civpsn civpsn, Date naissance,  String libcsp) {
	
		this.libdpt = libdpt;
		this.libsubcom = libsubcom;
		
		this.nompsn = nompsn;
		this.prepsn = prepsn;
		this.civpsn = civpsn;
		this.naissance = naissance;
		
		this.libcsp = libcsp;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}



	public String getLibdpt() {
		return libdpt;
	}

	public void setLibdpt(String libdpt) {
		this.libdpt = libdpt;
	}

	public String getLibsubcom() {
		return libsubcom;
	}

	public void setLibsubcom(String libsubcom) {
		this.libsubcom = libsubcom;
	}



	public String getNompsn() {
		return nompsn;
	}

	public void setNompsn(String nompsn) {
		this.nompsn = nompsn;
	}

	public String getPrepsn() {
		return prepsn;
	}

	public void setPrepsn(String prepsn) {
		this.prepsn = prepsn;
	}

	public Civpsn getCivpsn() {
		return civpsn;
	}

	public void setCivpsn(Civpsn civpsn) {
		this.civpsn = civpsn;
	}

	public Date getNaissance() {
		return naissance;
	}

	public void setNaissance(Date naissance) {
		this.naissance = naissance;
	}


	public String getLibcsp() {
		return libcsp;
	}

	public void setLibcsp(String libcsp) {
		this.libcsp = libcsp;
	}
	
	

	public Commune getCommune() {
		return commune;
	}

	public void setCommune(Commune commune) {
		this.commune = commune;
	}

	
	//+ ", naissance=" + naissance
	//+ commune.getNomCommune()
	@Override
	public String toString() {
		if(commune!=null)
		return "Maire [commune="+ (commune.getNomCommune().isEmpty()? "none": commune.getNomCommune())+  ", id=" + id + ", libdpt=" + libdpt + ", libsubcom=" + libsubcom
				+ ", nompsn=" + nompsn + ", prepsn=" + prepsn + ", civpsn=" + civpsn 
				+ ", libcsp=" + libcsp + "]";
		else
			return "Maire [id=" + id + ", libdpt=" + libdpt + ", libsubcom=" + libsubcom
					+ ", nompsn=" + nompsn + ", prepsn=" + prepsn + ", civpsn=" + civpsn 
					+ ", libcsp=" + libcsp + "]";
	}

	
	
	
	//SimpleDateFormat
}


