package org.nedj.jpa.exo2;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.nedj.jpa.exo4.Maire;

public class MainCommune {

	public static void main(String[] args)  throws IOException {
		
		
		// with create in persistence 
		
		String file="src/main/resources/files/laposte_hexasmal.csv";
	
		
		String PersistenceUnitName="tp-jpa-hibernate-create-commune";
		//point d entree de jpa
		EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory(PersistenceUnitName);
		System.out.println(entityManagerFactory);
		EntityManager entityManager= entityManagerFactory.createEntityManager();
		
		
		CommuneDBService cs=new CommuneDBImpl(entityManager);
		
		//La base est grande elle prend beaucoup de temps pour se charger
		//je me limite � un petit nombre 
		//importer
		
		entityManager.getTransaction().begin();
		
		CommuneImporter ci= new CommuneCSVImporter(cs);
		ci.importCommunes(file);
		
		entityManager.getTransaction().commit();

		
		//select avec un id
		System.out.println("Commune avec id=10: "+cs.getCommuneById(10L));
		
		//select avec un name
		System.out.println("Commune avec nom=NANCY: "+cs.getCommuneByName("BOZ"));
		
		//compter le nombre de communes
		System.out.println("Nombre de communes avec dans le d�partement 32: "+cs.countCommuneByCP("32"));
	
		//setting 
				List<Commune> communes =cs.getCommunes();
				communes.stream().limit(20).forEach(System.out::println);
	
	
	}
	

}
