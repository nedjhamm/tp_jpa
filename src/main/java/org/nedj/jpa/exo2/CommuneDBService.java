package org.nedj.jpa.exo2;

import java.util.List;



public interface CommuneDBService {

	boolean writeCommune(Commune commune);
	Commune getCommuneById(long id);
	Commune getCommuneByName(String name);
	public int countCommuneByCP(String codePostal);
	public List<Commune> getCommunes();
}
