package org.nedj.jpa.exo2;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class CreateTableCommune {

	public static void main(String[] args) {
		
		
		String PersistenceUnitName="tp-jpa-hibernate-create-commune";
		EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory(PersistenceUnitName);
		System.out.println(entityManagerFactory);
	}

}
