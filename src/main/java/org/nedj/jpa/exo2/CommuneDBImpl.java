package org.nedj.jpa.exo2;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.nedj.jpa.exo4.Maire;



public class CommuneDBImpl implements CommuneDBService {
	
	private EntityManager entityManager;

	public CommuneDBImpl(EntityManager entityManager) {
		this.entityManager=entityManager;
	}

	@Override
	public boolean writeCommune(Commune commune) {
		
		
			entityManager.persist(commune);
		
		
		return true;
	}

	@Override
	public Commune getCommuneById(long id) {
		
		Commune commune=entityManager.find(Commune.class, id);
		
		return commune;
	}

	@Override
	public Commune getCommuneByName(String name) {
		
		Query query= entityManager.createNamedQuery("Commune.byName");
		query.setParameter("name", name);
		return (Commune) query.getSingleResult();
		
	}

	@Override
	public int countCommuneByCP(String codePostal) {
		
		Query query= entityManager.createNamedQuery("Commune.byCode");
		query.setParameter("prefix", codePostal+"%");
		List<Commune> communes= (List<Commune>) query.getResultList();
		return communes.size();
	}
	
	@Override
	public List<Commune> getCommunes()
	{
		Query query= entityManager.createNamedQuery("Commune.all");
		List<Commune> communes= (List<Commune>) query.getResultList();
		return communes;
	}

}
