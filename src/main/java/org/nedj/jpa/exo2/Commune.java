package org.nedj.jpa.exo2;


import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.nedj.jpa.exo4.Maire;

@Entity(name="Commune")

@NamedQueries(
		{
		@NamedQuery(name="Commune.byName",query="select c from Commune c where c.nomCommune=:name"),
		@NamedQuery(name="Commune.byCode",query="select c from Commune c where c.codePostal like :prefix"),
		@NamedQuery(name="Commune.all", query="select c from Commune c")
		
		})


@Table(name="Commune")
public class Commune implements Serializable{

	
	@OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH},
			fetch=FetchType.EAGER
			)
	private Maire maire;
	
		@Id @GeneratedValue(strategy = GenerationType.AUTO)
		private long id;
		
		@Column(length=40)
		private String codeINSEE;
		
		@Column(name="code_postal" ,length=40)
		private String codePostal;
		
		@Column(length= 40)
		private String nomCommune;

		@Column(length= 40)
		private String libelleAcheminement;

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public String getCodeINSEE() {
			return codeINSEE;
		}

		public void setCodeINSEE(String codeINSEE) {
			this.codeINSEE = codeINSEE;
		}

		public String getCodePostal() {
			return codePostal;
		}

		public void setCodePostal(String codePostal) {
			this.codePostal = codePostal;
		}

		public String getNomCommune() {
			return nomCommune;
		}

		public void setNomCommune(String nomCommune) {
			this.nomCommune = nomCommune;
		}

		public String getLibelleAcheminement() {
			return libelleAcheminement;
		}

		public void setLibelleAcheminement(String libelleAcheminement) {
			this.libelleAcheminement = libelleAcheminement;
		}

		

		public Maire getMaire() {
			return maire;
		}

		
		@Override
		public String toString() {
			if(maire!=null)
			return "Commune [maire=" + (maire.getNompsn().isEmpty() ? "none" : maire.getNompsn() )
					+" "+ (maire.getPrepsn().isEmpty()? "none": maire.getPrepsn())+ ", id=" + id 
					+ ", codeINSEE=" + codeINSEE + ", codePostal=" + codePostal
					+ ", nomCommune=" + nomCommune + ", libelleAcheminement=" + libelleAcheminement + "]";
			else
				return "Commune [ id=" + id 
						+ ", codeINSEE=" + codeINSEE + ", codePostal=" + codePostal
						+ ", nomCommune=" + nomCommune + ", libelleAcheminement=" + libelleAcheminement + "]";
				
				
		}

		public void setMaire(Maire maire) {
			this.maire = maire;
		}

		public Commune(String codeINSEE, String codePostal, String nomCommune, String libelleAcheminement) {
			
			this.codeINSEE = codeINSEE;
			this.codePostal = codePostal;
			this.nomCommune = nomCommune;
			this.libelleAcheminement = libelleAcheminement;
		}

		public Commune() {
		
		}
		
		
				
}
		
